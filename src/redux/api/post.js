import axios from './index';

export async function posts(data){
    return axios.post('post/create',data);
}

export async function getPosts(data){
    return axios.get(`post/list/${data.filter}`, {
        params: {
            search: data.search
        }
    })
}
